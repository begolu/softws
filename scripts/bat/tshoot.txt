
Configuration IP de Windows

   Nom de l'h�te . . . . . . . . . . : asusi7
   Suffixe DNS principal . . . . . . : 
   Type de noeud. . . . . . . . . .  : Hybride
   Routage IP activ� . . . . . . . . : Non
   Proxy WINS activ� . . . . . . . . : Non
   Liste de recherche du suffixe DNS.: home

Carte Ethernet Connexion r�seau Bluetooth :

   Statut du m�dia. . . . . . . . . . . . : M�dia d�connect�
   Suffixe DNS propre � la connexion. . . : 
   Description. . . . . . . . . . . . . . : Bluetooth Device (Personal Area Network)
   Adresse physique . . . . . . . . . . . : 54-27-1E-75-55-DE
   DHCP activ�. . . . . . . . . . . . . . : Oui
   Configuration automatique activ�e. . . : Oui

Carte r�seau sans fil Connexion au r�seau local* 2�:

   Statut du m�dia. . . . . . . . . . . . : M�dia d�connect�
   Suffixe DNS propre � la connexion. . . : 
   Description. . . . . . . . . . . . . . : Microsoft Wi-Fi Direct Virtual Adapter
   Adresse physique . . . . . . . . . . . : 16-27-1E-75-55-DF
   DHCP activ�. . . . . . . . . . . . . . : Oui
   Configuration automatique activ�e. . . : Oui

Carte r�seau sans fil Wi-Fi�:

   Suffixe DNS propre � la connexion. . . : home
   Description. . . . . . . . . . . . . . : Qualcomm Atheros AR9485WB-EG Wireless Network Adapter
   Adresse physique . . . . . . . . . . . : 54-27-1E-75-55-DF
   DHCP activ�. . . . . . . . . . . . . . : Oui
   Configuration automatique activ�e. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::196d:fe54:cb62:2004%4(pr�f�r�) 
   Adresse IPv4. . . . . . . . . . . . . .: 10.53.213.4(pr�f�r�) 
   Masque de sous-r�seau. . . .�. . . . . : 255.255.255.0
   Bail obtenu. . . . . . . . .�. . . . . : dimanche 5 avril 2015 06:55:10
   Bail expirant. . . . . . . . .�. . . . : lundi 6 avril 2015 06:55:13
   Passerelle par d�faut. . . .�. . . . . : 10.53.213.250
   Serveur DHCP . . . . . . . . . . . . . : 10.53.213.250
   IAID DHCPv6 . . . . . . . . . . . : 72623902
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-1B-B0-63-96-40-16-7E-8B-56-1F
   Serveurs DNS. . .  . . . . . . . . . . : 10.53.213.250
	                               10.53.213.250
   NetBIOS sur Tcpip. . . . . . . . . . . : Activ�

Carte Ethernet Ethernet :

   Statut du m�dia. . . . . . . . . . . . : M�dia d�connect�
   Suffixe DNS propre � la connexion. . . : 
   Description. . . . . . . . . . . . . . : Realtek PCIe GBE Family Controller
   Adresse physique . . . . . . . . . . . : 40-16-7E-8B-56-1F
   DHCP activ�. . . . . . . . . . . . . . : Non
   Configuration automatique activ�e. . . : Oui

Carte Ethernet VirtualBox Host-Only Network :

   Suffixe DNS propre � la connexion. . . : 
   Description. . . . . . . . . . . . . . : VirtualBox Host-Only Ethernet Adapter
   Adresse physique . . . . . . . . . . . : 08-00-27-00-6C-C3
   DHCP activ�. . . . . . . . . . . . . . : Non
   Configuration automatique activ�e. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::2171:7fdb:2a83:bea8%27(pr�f�r�) 
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.56.1(pr�f�r�) 
   Masque de sous-r�seau. . . .�. . . . . : 255.255.255.0
   Passerelle par d�faut. . . .�. . . . . : 
   IAID DHCPv6 . . . . . . . . . . . : 520618023
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-1B-B0-63-96-40-16-7E-8B-56-1F
   Serveurs DNS. . .  . . . . . . . . . . : fec0:0:0:ffff::1%1
	                               fec0:0:0:ffff::2%1
	                               fec0:0:0:ffff::3%1
   NetBIOS sur Tcpip. . . . . . . . . . . : Activ�

Carte Tunnel isatap.home :

   Statut du m�dia. . . . . . . . . . . . : M�dia d�connect�
   Suffixe DNS propre � la connexion. . . : 
   Description. . . . . . . . . . . . . . : Carte Microsoft ISATAP
   Adresse physique . . . . . . . . . . . : 00-00-00-00-00-00-00-E0
   DHCP activ�. . . . . . . . . . . . . . : Non
   Configuration automatique activ�e. . . : Oui

Carte Tunnel Teredo Tunneling Pseudo-Interface :

   Suffixe DNS propre � la connexion. . . : 
   Description. . . . . . . . . . . . . . : Teredo Tunneling Pseudo-Interface
   Adresse physique . . . . . . . . . . . : 00-00-00-00-00-00-00-E0
   DHCP activ�. . . . . . . . . . . . . . : Non
   Configuration automatique activ�e. . . : Oui
   Adresse IPv6. . . . . . . . . . .�. . .: 2001:0:9d38:6abd:cad:31d5:a5ca:17aa(pr�f�r�) 
   Adresse IPv6 de liaison locale. . . . .: fe80::cad:31d5:a5ca:17aa%9(pr�f�r�) 
   Passerelle par d�faut. . . .�. . . . . : ::
   IAID DHCPv6 . . . . . . . . . . . : 352321536
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-1B-B0-63-96-40-16-7E-8B-56-1F
   NetBIOS sur TCPIP. . . . . . . . . . . : D�sactiv�

Carte Tunnel isatap.{0FB53F14-A32A-4F4B-8556-4745632BAD4A} :

   Statut du m�dia. . . . . . . . . . . . : M�dia d�connect�
   Suffixe DNS propre � la connexion. . . : 
   Description. . . . . . . . . . . . . . : Carte Microsoft ISATAP #3
   Adresse physique . . . . . . . . . . . : 00-00-00-00-00-00-00-E0
   DHCP activ�. . . . . . . . . . . . . . : Non
   Configuration automatique activ�e. . . : Oui
===========================================================================
Liste d'Interfaces
  6...54 27 1e 75 55 de ......Bluetooth Device (Personal Area Network)
  5...16 27 1e 75 55 df ......Microsoft Wi-Fi Direct Virtual Adapter
  4...54 27 1e 75 55 df ......Qualcomm Atheros AR9485WB-EG Wireless Network Adapter
  3...40 16 7e 8b 56 1f ......Realtek PCIe GBE Family Controller
 27...08 00 27 00 6c c3 ......VirtualBox Host-Only Ethernet Adapter
  1...........................Software Loopback Interface 1
  8...00 00 00 00 00 00 00 e0 Carte Microsoft ISATAP
  9...00 00 00 00 00 00 00 e0 Teredo Tunneling Pseudo-Interface
 10...00 00 00 00 00 00 00 e0 Carte Microsoft ISATAP #3
===========================================================================

IPv4 Table de routage
===========================================================================
Itin�raires actifs�:
Destination r�seau    Masque r�seau  Adr. passerelle   Adr. interface M�trique
          0.0.0.0          0.0.0.0    10.53.213.250      10.53.213.4     25
      10.53.213.0    255.255.255.0         On-link       10.53.213.4    281
      10.53.213.4  255.255.255.255         On-link       10.53.213.4    281
    10.53.213.255  255.255.255.255         On-link       10.53.213.4    281
        127.0.0.0        255.0.0.0         On-link         127.0.0.1    306
        127.0.0.1  255.255.255.255         On-link         127.0.0.1    306
  127.255.255.255  255.255.255.255         On-link         127.0.0.1    306
     192.168.56.0    255.255.255.0         On-link      192.168.56.1    276
     192.168.56.1  255.255.255.255         On-link      192.168.56.1    276
   192.168.56.255  255.255.255.255         On-link      192.168.56.1    276
        224.0.0.0        240.0.0.0         On-link         127.0.0.1    306
        224.0.0.0        240.0.0.0         On-link      192.168.56.1    276
        224.0.0.0        240.0.0.0         On-link       10.53.213.4    281
  255.255.255.255  255.255.255.255         On-link         127.0.0.1    306
  255.255.255.255  255.255.255.255         On-link      192.168.56.1    276
  255.255.255.255  255.255.255.255         On-link       10.53.213.4    281
===========================================================================
Itin�raires persistants�:
  Adresse r�seau    Masque r�seau  Adresse passerelle M�trique
          0.0.0.0          0.0.0.0    10.53.213.250  Par d�faut 
===========================================================================

IPv6 Table de routage
===========================================================================
Itin�raires actifs�:
 If Metric Network Destination      Gateway
  9    306 ::/0                     On-link
  1    306 ::1/128                  On-link
  9    306 2001::/32                On-link
  9    306 2001:0:9d38:6abd:cad:31d5:a5ca:17aa/128
                                    On-link
 27    276 fe80::/64                On-link
  4    281 fe80::/64                On-link
  9    306 fe80::/64                On-link
  9    306 fe80::cad:31d5:a5ca:17aa/128
                                    On-link
  4    281 fe80::196d:fe54:cb62:2004/128
                                    On-link
 27    276 fe80::2171:7fdb:2a83:bea8/128
                                    On-link
  1    306 ff00::/8                 On-link
 27    276 ff00::/8                 On-link
  9    306 ff00::/8                 On-link
  4    281 ff00::/8                 On-link
===========================================================================
Itin�raires persistants�:
  Aucun
Statistiques de station de \\ASUSI7


Statistiques depuis 04/04/2015 19:58:33


  Octets re�us                   71701
  Blocs SMB re�us                89
  Octets envoy�s                 65000
  Blocs SMB envoy�s              27
  Lectures                       36
  �critures                      0
  Refus de lectures brutes       0
  Refus d'�critures brutes       0

  Erreurs r�seau                 0
  Connexions �tablies            3
  Reconnexions                   0
  D�connexions automatiques      0

  Sessions ouvertes              2
  Sessions bloqu�es              0
  �checs de sessions             0
  �checs d'op�rations            0
  Nb. d'utilisations             8
  Nb. d'�checs d'utilisation     0

La commande s'est termin�e correctement.

