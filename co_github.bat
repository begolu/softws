@echo OFF

set DRYVE=%1

set REPSEP=_.-
set REPERT=HG_SVN

%DRYVE%:
if not exist %DRYVE%:\%REPERT% mkdir %DRYVE%:\%REPERT%
cd %DRYVE%:\%REPERT%  

set PROPRIO=10up
set REPOSI=varying-vagrant-vagrants
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
if exist .gitmodules git submodule init
if exist .gitmodules git submodule update
cd %DRYVE%:\%REPERT%




set PROPRIO=aclark4life
set REPOSI=collective.recipe.bluebream
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
if exist .gitmodules git submodule init
if exist .gitmodules git submodule update
cd %DRYVE%:\%REPERT%

set REPOSI=plone-install
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
if exist .gitmodules git submodule init
if exist .gitmodules git submodule update
cd %DRYVE%:\%REPERT%

set REPOSI=vanity
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
if exist .gitmodules git submodule init
if exist .gitmodules git submodule update
cd %DRYVE%:\%REPERT%


set PROPRIO=aussielunix
set REPOSI=graylog2-appliance
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
if exist .gitmodules git submodule init
if exist .gitmodules git submodule update
cd %DRYVE%:\%REPERT%

rem set REPOSI=puppet-project_example
rem echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
rem if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
rem if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
rem if not exist co.bat git fetch
if exist .gitmodules git submodule init
if exist .gitmodules git submodule update
rem cd %DRYVE%:\%REPERT%


set PROPRIO=bdossantos
set REPOSI=puppet-module-couchdb
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
if exist .gitmodules git submodule init
if exist .gitmodules git submodule update
cd %DRYVE%:\%REPERT%





set PROPRIO=bekkopen
set REPOSI=jetty-pkg
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
if exist .gitmodules git submodule init
if exist .gitmodules git submodule update
cd %DRYVE%:\%REPERT%


set PROPRIO=benoitbryon
set REPOSI=python-buildout-directories
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
if exist .gitmodules git submodule init
if exist .gitmodules git submodule update
cd %DRYVE%:\%REPERT%
set REPOSI=documentation-style-guide-sphinx
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
if exist .gitmodules git submodule init
if exist .gitmodules git submodule update
cd %DRYVE%:\%REPERT%
set REPOSI=rst2rst
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
if exist .gitmodules git submodule init
if exist .gitmodules git submodule update
cd %DRYVE%:\%REPERT%


set PROPRIO=BetterBrief
set REPOSI=vagrant-skeleton
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
if exist .gitmodules git submodule init
if exist .gitmodules git submodule update
cd %DRYVE%:\%REPERT%



set PROPRIO=collective
set REPOSI=collective.developermanual
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%
set REPOSI=collective.project
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%
set REPOSI=templer.core
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%

set REPOSI=templer.buildout
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%
set REPOSI=templer.plone
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%



set REPOSI=templer.zope
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%



set PROPRIO=DasIch
set REPOSI=brownie
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%
set REPOSI=sphinx-theme-minimalism
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%


set PROPRIO=datasciencemasters
set REPOSI=go
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
if exist .gitmodules git submodule init
if exist .gitmodules git submodule update
cd %DRYVE%:\%REPERT%




set PROPRIO=discourse
set REPOSI=discourse
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%

set PROPRIO=dotcloud
set REPOSI=docker
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
if exist .gitmodules git submodule init
if exist .gitmodules git submodule update
cd %DRYVE%:\%REPERT%
set REPOSI=docker-tutorial
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
if exist .gitmodules git submodule init
if exist .gitmodules git submodule update
cd %DRYVE%:\%REPERT%
set REPOSI=www.docker.io
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
if exist .gitmodules git submodule init
if exist .gitmodules git submodule update
cd %DRYVE%:\%REPERT%




set PROPRIO=example42
set REPOSI=puppet-modules
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
if exist .gitmodules git submodule init
if exist .gitmodules git submodule update
cd %DRYVE%:\%REPERT%
set REPOSI=puppet-modules-nextgen
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
if exist .gitmodules git submodule init
if exist .gitmodules git submodule update
cd %DRYVE%:\%REPERT%
set REPOSI=puppet-playground
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
if exist .gitmodules git submodule init
if exist .gitmodules git submodule update
cd %DRYVE%:\%REPERT%


set PROPRIO=facebook
set REPOSI=tornado
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%


set PROPRIO=grahamgilbert
set REPOSI=vagrant-puppetmaster
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
if exist .gitmodules git submodule init
if exist .gitmodules git submodule update
cd %DRYVE%:\%REPERT%
set REPOSI=puppet_psu_2013
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
if exist .gitmodules git submodule init
if exist .gitmodules git submodule update
cd %DRYVE%:\%REPERT%


set PROPRIO=heavywater
set REPOSI=pennyworth
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%
set REPOSI=pennyworth-packages
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%

set REPOSI=echelon
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%
 
set PROPRIO=iJoyCode
set REPOSI=vagrant-puppet-centos-php-apache
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
if exist .gitmodules git submodule init
if exist .gitmodules git submodule update
cd %DRYVE%:\%REPERT%




set PROPRIO=ipython
set REPOSI=ipython
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%



set PROPRIO=jenkinsci
set REPOSI=infra-puppet
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%


set PROPRIO=jgm
set REPOSI=yst
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%
set REPOSI=pandoc
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%
set REPOSI=gitit
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%
set REPOSI=pandoc-templates
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%

rem goto lafin

set PROPRIO=kennethreitz
set REPOSI=requests
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%

set REPOSI=legit
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%

set REPOSI=git-legit.org
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%
set REPOSI=envoy
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%
set REPOSI=python-guide
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%



rem  klen (Kirill Klenov)
rem Email horneds@gmail.com
rem Website/Blog http://klen.github.com
rem Russia, Moscow
rem Member Since Jun 01, 2009

set PROPRIO=klen
set REPOSI=makesite
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%
set REPOSI=klen.github.com
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%



set PROPRIO=KrisBuytaert
set REPOSI=vagrant-graylog2
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%


set PROPRIO=luispedro
set REPOSI=django-gitcms
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%

set PROPRIO=marcojanssen
set REPOSI=vagrant-puppet-jenkins
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
if exist .gitmodules git submodule init
if exist .gitmodules git submodule update
cd %DRYVE%:\%REPERT%

set REPOSI=varnishtraining
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
if exist .gitmodules git submodule init
if exist .gitmodules git submodule update
cd %DRYVE%:\%REPERT%



set PROPRIO=mgenti
set REPOSI=tornado
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%

set PROPRIO=michaelklishin
set REPOSI=sous-chef
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%
set REPOSI=neo4j-server-chef-cookbook
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%


set PROPRIO=misja
set REPOSI=python-boilerpipe
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
if exist .gitmodules git submodule init
if exist .gitmodules git submodule update
cd %DRYVE%:\%REPERT%




set PROPRIO=mitsuhiko
set REPOSI=logbook
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
if exist .gitmodules git submodule init
if exist .gitmodules git submodule update
cd %DRYVE%:\%REPERT%

set PROPRIO=mitsuhiko
set REPOSI=werkzeug
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%
set REPOSI=flask
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%
set REPOSI=jinja2
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%




set PROPRIO=montylounge
set REPOSI=django-mingus
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%




set PROPRIO=mozilla
set REPOSI=playdoh
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%
set REPOSI=zamboni
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%





set PROPRIO=practicalweb
set REPOSI=vagrant-rpmbuild
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
if exist .gitmodules git submodule init
if exist .gitmodules git submodule update
cd %DRYVE%:\%REPERT%


set PROPRIO=ptahproject
set REPOSI=ptah
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%

set PROPRIO=ptwobrussell
set REPOSI=Mining-the-Social-Web-2nd-Edition
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
if exist .gitmodules git submodule init
if exist .gitmodules git submodule update
cd %DRYVE%:\%REPERT%


set PROPRIO=pypa
set REPOSI=virtualenv
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%
set REPOSI=pip
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%

set PROPRIO=qtproject
set REPOSI=qtqa-sysadmin
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%


set PROPRIO=rescrv
set REPOSI=firmant
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%




set PROPRIO=ripienaar
set REPOSI=puppet-concat
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%

set PROPRIO=sgphpug
set REPOSI=puppet-server
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
if exist .gitmodules git submodule init
if exist .gitmodules git submodule update
cd %DRYVE%:\%REPERT%

set REPOSI=vagrant-lamp-centos64
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
if exist .gitmodules git submodule init
if exist .gitmodules git submodule update
cd %DRYVE%:\%REPERT%





set PROPRIO=shanx
set REPOSI=amplecode.recipe.template
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%



set PROPRIO=Simplistix
set REPOSI=buildout-tox
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%
set REPOSI=buildout-versions
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%


set PROPRIO=skorokithakis
set REPOSI=episode-renamer
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%
set REPOSI=back-scratcher
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%
set REPOSI=omnisync
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%
set REPOSI=mkproject
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%
set REPOSI=imdbapi
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%


set PROPRIO=swaroopch
set REPOSI=byte_of_python
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
if exist .gitmodules git submodule init
if exist .gitmodules git submodule update
cd %DRYVE%:\%REPERT%
set REPOSI=datafaker
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
if exist .gitmodules git submodule init
if exist .gitmodules git submodule update
cd %DRYVE%:\%REPERT%
set REPOSI=edn_format
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
if exist .gitmodules git submodule init
if exist .gitmodules git submodule update
cd %DRYVE%:\%REPERT%




set PROPRIO=technomancy
set REPOSI=emacs-starter-kit
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%

set PROPRIO=torgeilo
set REPOSI=amplecode.recipe.template
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%


set PROPRIO=thucydides-webtests
set REPOSI=thucydides
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%

set PROPRIO=toastdriven
set REPOSI=microsearch
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%

set PROPRIO=tpope
set REPOSI=vim-fugitive
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%

set PROPRIO=travis-ci
set REPOSI=travis-cookbooks
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%
set REPOSI=travis-boxes
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%
set REPOSI=travis-ci.github.com
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
cd %DRYVE%:\%REPERT%

set PROPRIO=tsteur
set REPOSI=django-dev-vm
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
if exist .gitmodules git submodule init
if exist .gitmodules git submodule update
cd %DRYVE%:\%REPERT%
set REPOSI=php-dev-vm
echo ---------------------  %PROPRIO%%REPSEP%%REPOSI%
if not exist %PROPRIO%%REPSEP%%REPOSI%_git-dev git clone https://github.com/%PROPRIO%/%REPOSI% %PROPRIO%%REPSEP%%REPOSI%_git-dev
if  exist %PROPRIO%%REPSEP%%REPOSI%_git-dev cd %PROPRIO%%REPSEP%%REPOSI%_git-dev 
if not exist co.bat git fetch
if exist .gitmodules git submodule init
if exist .gitmodules git submodule update
cd %DRYVE%:\%REPERT%



:lafin
